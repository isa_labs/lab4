source /software/scripts/init_questa10.7c

rm -r work
vlib work

#compile
vcom -93 -work ./work ../src/flipflopD.vhd
vcom -93 -work ./work ../src/general_register.vhd
vcom -93 -work ./work ../src/*.vhd

vcom -93 -work ./work ../src/fpuvhdl/adder/fpalign_struct.vhd
vcom -93 -work ./work ../src/fpuvhdl/adder/fpinvert_fpinvert.vhd
vcom -93 -work ./work ../src/fpuvhdl/adder/fplzc_fplzc.vhd
vcom -93 -work ./work ../src/fpuvhdl/adder/fpselcomplement_fpselcomplement.vhd
vcom -93 -work ./work ../src/fpuvhdl/adder/fpswap_fpswap.vhd
vcom -93 -work ./work ../src/fpuvhdl/common/fpnormalize_fpnormalize.vhd
vcom -93 -work ./work ../src/fpuvhdl/common/fpround_fpround.vhd
vcom -93 -work ./work ../src/fpuvhdl/common/packfp_packfp.vhd
vcom -93 -work ./work ../src/fpuvhdl/common/unpackfp_unpackfp.vhd
vcom -93 -work ./work ../src/fpuvhdl/adder/fpadd_normalize_struct.vhd

vcom -93 -work ./work ../src/fpuvhdl/adder/fpadd_single_cycle.vhd
vcom -93 -work ./work ../src/fpuvhdl/adder/fpadd_stage1_struct.vhd
vcom -93 -work ./work ../src/fpuvhdl/adder/fpadd_stage2_struct.vhd
vcom -93 -work ./work ../src/fpuvhdl/adder/fpadd_stage3_struct.vhd
vcom -93 -work ./work ../src/fpuvhdl/adder/fpadd_stage4_struct.vhd
vcom -93 -work ./work ../src/fpuvhdl/adder/fpadd_stage5_struct.vhd
vcom -93 -work ./work ../src/fpuvhdl/adder/fpadd_stage6_struct.vhd
vcom -93 -work ./work ../src/fpuvhdl/adder/fpadd_pipeline.vhd

vcom -93 -work ./work ../src/fpuvhdl/multiplier/fpmul_single_cycle.vhd
vcom -93 -work ./work ../src/fpuvhdl/multiplier/fpmul_stage1_struct.vhd
vcom -93 -work ./work ../src/fpuvhdl/multiplier/fpmul_stage2_struct.vhd
vcom -93 -work ./work ../src/fpuvhdl/multiplier/fpmul_stage3_struct.vhd
vcom -93 -work ./work ../src/fpuvhdl/multiplier/fpmul_stage4_struct.vhd
vcom -93 -work ./work ../src/fpuvhdl/multiplier/fpmul_pipeline.vhd

vlog -sv ../tb/top.sv

#start simulation
vsim top


