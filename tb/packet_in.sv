class packet_in extends uvm_sequence_item;
     rand integer A;
    //  constraint my_range {A inside {[100:1000]};}  //set A between 100 and 1000
    rand integer B;
      constraint co { A inside {[32'h25400003:32'ha6900000]} & B inside {[32'h25400003:32'ha6900000]};} 
      

    `uvm_object_utils_begin(packet_in)
        `uvm_field_int(A, UVM_ALL_ON|UVM_HEX)
        `uvm_field_int(B, UVM_ALL_ON|UVM_HEX)
    `uvm_object_utils_end

    function new(string name="packet_in");
        super.new(name);
    endfunction: new
endclass: packet_in
