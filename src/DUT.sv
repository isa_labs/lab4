// module DUT(dut_if.port_in in_inter, dut_if.port_out out_inter, output enum logic [1:0] {INITIAL,WAIT,SEND} state);
module DUT(dut_if.port_in in_inter, dut_if.port_out out_inter, output enum logic [2:0] {INITIAL,WAIT1,WAIT2,WAIT3,WAIT4,SEND} state);
   
integer counter;

// adder adder_under_test(.A(in_inter.A),.B(in_inter.B),.OUT(out_inter.data));
// Mult_Dadda32 dadda_under_test(.x(in_inter.A),.y(in_inter.B),.z(out_inter.data));
// FPmul_single_cycle floating_point_mul_under_test(.FP_A(in_inter.A),.FP_B(in_inter.B),.clk(in_inter.clk),.FP_Z(out_inter.data));
FPmul floating_point_mul_under_test(.FP_A(in_inter.A),.FP_B(in_inter.B),.clk(in_inter.clk),.FP_Z(out_inter.data));
  
always_ff @(posedge in_inter.clk)

	begin
		
		if(in_inter.rst) begin
			in_inter.ready <= 0;
			out_inter.data <= 'x;
			out_inter.valid <= 0;
			assign counter = 0;
			state <= INITIAL;
		end
		else case(state)
			INITIAL: begin
				in_inter.ready <= 1;
				state <= WAIT1;
				//state <= WAIT;
			end

			//WAIT: begin
				//if(in_inter.valid) begin
					//in_inter.ready <= 0;		
					//out_inter.data <= in_inter.A + in_inter.B;
					//out_inter.data=in_inter.A * in_inter.B;
					//$display("Iteration %d. Dadda_mult: input A = %f, input B = %f, output OUT = %f",counter,$bitstoshortreal(in_inter.A),$bitstoshortreal(in_inter.B),$bitstoshortreal(out_inter.data));
					//$display("Iteration %d. Dadda_mult: input A = %b, input B = %b, output OUT = %b",counter,in_inter.A,in_inter.B,out_inter.data);
					//out_inter.valid <= 1;
					//assign counter = counter + 1;
					//state <= SEND;
				//end
			//end

			WAIT1: begin
				if(in_inter.valid) begin
					//$display("Dadda_mult: input A = %d, input B = %d, output OUT = %d",in_inter.A,in_inter.B,out_inter.data);
					$display("WAIT1 FP_mult: input A = %h, input B = %h, output OUT = %h",in_inter.A,in_inter.B,out_inter.data);					
					in_inter.ready <= 0;
					state <= WAIT2;
				end
			end 

			WAIT2: begin	 
				//$display("Dadda_mult: input A = %d, input B = %d, output OUT = %d",in_inter.A,in_inter.B,out_inter.data);
				$display("WAIT2 FP_mult: input A = %h, input B = %h, output OUT = %h",in_inter.A,in_inter.B,out_inter.data);
				state <= WAIT3;
			end

			WAIT3: begin
				//$display("Dadda_mult: input A = %d, input B = %d, output OUT = %d",in_inter.A,in_inter.B,out_inter.data);
				$display("WAIT3 FP_mult: input A = %h, input B = %h, output OUT = %h",in_inter.A,in_inter.B,out_inter.data);
				state <= WAIT4;
			end
	
			WAIT4: begin
				//if(in_inter.valid) begin
				//in_inter.ready <= 0;
				//out_inter.data <= in_inter.A + in_inter.B;
				//out_inter.data=in_inter.A * in_inter.B;
				//$display("Dadda_mult: input A = %d, input B = %d, output OUT = %d",in_inter.A,in_inter.B,out_inter.data);
				$display("WAIT4 FP_mult: input A = %h, input B = %h, output OUT = %h",in_inter.A,in_inter.B,out_inter.data);
				out_inter.valid <= 1;
				state <= SEND;
				//end
			end

			SEND: begin
				if(out_inter.ready) begin
					out_inter.valid <= 0;
					in_inter.ready <= 1;
					$display("SEND FP_mult: input A = %h, input B = %h, output OUT = %h",in_inter.A,in_inter.B,out_inter.data);
					state <= WAIT1;
					//state <= WAIT;
				end
			end
		endcase
	end
endmodule: DUT
