library ieee;
use ieee.std_logic_1164.all;

entity general_register  is   --asynchronous N bit register
	generic(N : integer := 32);
	port (D :	in std_logic_vector(N-1 downto 0);
				clk :	in std_logic;
				rst :	in std_logic;
				en : in std_logic;
				Q :	out	std_logic_vector(N-1 downto 0));
end general_register;

architecture behav of general_register is

	component flipflopD
		port (D :	in std_logic;
					clk :	in std_logic;
					rst :	in std_logic;
					en : in std_logic;
					Q :	out	std_logic);
	end component;

begin

	reg : for i in 0 to N-1 generate
		FFDtype : flipflopD
			port map (D => D(i),
								clk => clk,
								rst => rst,
								en => en,
								Q => Q(i));
	end generate;

end behav;
