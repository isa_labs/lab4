class refmod extends uvm_component;
    `uvm_component_utils(refmod)
    
    packet_in tr_in;
    packet_out tr_out;
    uvm_get_port #(packet_in) in;
    uvm_put_port #(packet_out) out;
     
    	
	integer counter = 0;
    
    function new(string name = "refmod", uvm_component parent);
        super.new(name, parent);
        in = new("in", this);
        out = new("out", this);
    endfunction
    
    virtual function void build_phase(uvm_phase phase);
        super.build_phase(phase);
        tr_out = packet_out::type_id::create("tr_out", this);
    endfunction: build_phase
    
    virtual task run_phase(uvm_phase phase);
        super.run_phase(phase);
        
        forever begin
            
            in.get(tr_in);
          // tr_out.data = tr_in.A + tr_in.B; //for the adder
              // tr_out.data = tr_in.A - tr_in.B; 
             tr_out.data = $shortrealtobits($bitstoshortreal(tr_in.A) * $bitstoshortreal(tr_in.B));   //for the flaoting point mult
            
          //  $display("refmod: input A = %f, input B = %f, output OUT = %f",$bitstoshortreal(tr_in.A), $bitstoshortreal(tr_in.B), $bitstoshortreal(tr_out.data));
			$display("refmod: input A = %h, input B = %h, output OUT = %h",tr_in.A, tr_in.B, tr_out.data);

			//assign counter = counter + 1;
			
            out.put(tr_out);
        end
    endtask: run_phase
endclass: refmod
