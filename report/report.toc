\babel@toc {english}{}
\contentsline {chapter}{\numberline {1}Verification with UVM}{1}{chapter.1}%
\contentsline {section}{\numberline {1.1}Verification of the adder}{1}{section.1.1}%
\contentsline {section}{\numberline {1.2}Verification of the MBE-Dadda tree multiplier}{2}{section.1.2}%
\contentsline {section}{\numberline {1.3}Verification of the entire floating point multiplier}{2}{section.1.3}%
\contentsline {section}{\numberline {1.4}Conclusion}{3}{section.1.4}%
